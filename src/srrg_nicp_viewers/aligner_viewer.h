#pragma once

#include <srrg_nicp/base_aligner.h>
#include <srrg_core_viewers/cloud_viewer.h>

namespace srrg_nicp_gui {
  
  class AlignerViewer : public srrg_core_viewers::Cloud3DViewer {

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  
    AlignerViewer(srrg_nicp::BaseAligner* aligner, float voxelResolution = 0.0);

    virtual void keyPressEvent(QKeyEvent* e);

    void align();

    void printErrorStats();

    inline float voxelResolution() const { return _voxelResolution; }
    inline srrg_nicp::BaseAligner* aligner() { return _aligner; }

    inline void setVoxelResolution(float voxelResolution) { _voxelResolution = voxelResolution; }
    inline void setAligner(srrg_nicp::BaseAligner* aligner) { _aligner = aligner; }
    
  protected:
    float _voxelResolution;
    srrg_nicp::BaseAligner* _aligner;

  };

}
