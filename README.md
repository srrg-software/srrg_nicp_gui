# srrg_nicp_gui

This package provides some graphical tools for 
[srrg_nicp](https://gitlab.com/srrg-software/srrg_nicp)

## Prerequisites

requires:
* [srrg_boss](https://gitlab.com/srrg-software/srrg_boss)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_core_map](https://gitlab.com/srrg-software/srrg_core_map)
* [srrg_core_map_gui](https://gitlab.com/srrg-software/srrg_core_map_gui)
* [srrg_nicp](https://gitlab.com/srrg-software/srrg_nicp)
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)

## Applications
`srrg_nicp_gui` provides some applications to perform cloud alignment.
These include:
* `srrg_nicp_projective_aligner_gui_app`: performs alignment using the projective aligner
* `srrg_nicp_spherical_aligner_gui_app`: performs alignment using the spherical aligner
* `srrg_nicp_nn_aligner_gui_app`: performs alignment using the nearest neighbor aligner

An help is available for each one of the app by typing `-h`, e.g.

    rosrun srrg_nicp_gui srrg_nicp_projective_aligner_gui_app -h

## Authors
* Jacopo Serafin
* Giorgio Grisetti

## Related Publications

* Jacopo Serafin and Giorgio Grisetti. "[**Using extended measurements and scene merging for efficient and robust point cloud registration**](http://www.sciencedirect.com/science/article/pii/S0921889015302712)", Robotics and Autonomous Systems, 2017.
* Jacopo Serafin and Giorgio Grisetti. "[**NICP: Dense Normal Based Point Cloud Registration.**](http://ieeexplore.ieee.org/document/7353455/)" In Proc. of the International Conference on Intelligent Robots and Systems (IROS), Hamburg, Germany, 2015.
